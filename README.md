# Recurrent Neural Network for Code Clone Detection

Code clones are a duplicated code which degrades the software quality and hence
increases the maintenance cost.
Code clone detection is an important problem for software maintenance and evolution.
Many researches have investigated different techniques to automatically detect
duplicate code in programs exceeding thousand lines of code. In this article we
propose an AI-based approach for detection of method-level clones in Java projects.
This approach uses a recurrent neural network with Siamese architecture for
clone detection.
A tool has been developed in Java and Python to support the experiments carried
out and to validate the proposed approach.
Finally, we provide some evaluation results and share the key stages of our future work.
