\include{preamble}

\begin{document}
\title{Recurrent Neural Network for Code Clone Detection}

\author{
	\IEEEauthorblockN{Arseny Zorin}
	\IEEEauthorblockA{Peter the Great St.-Petersburg\\Polytechnic University\\
		Saint-Petersburg, Russia \\
		E-mail: zorinarseny@yandex.ru}
	\and
	\IEEEauthorblockN{Vladimir Itsykson}
	\IEEEauthorblockA{Peter the Great St.-Petersburg\\Polytechnic University\\
		Saint-Petersburg, Russia\\
		E-mail: vlad@icc.spbstu.ru}
}

\maketitle

\begin{abstract}

	Code clones are a duplicated code which degrades the software quality and hence
	increases the maintenance cost.
	Code clone detection is an important problem for software maintenance and evolution.
	Many researches have investigated different techniques to automatically detect
	duplicate code in programs exceeding thousand lines of code. In this article we
	propose an AI-based approach for detection of method-level clones in Java projects.
	This approach uses a recurrent neural network with Siamese architecture for
	clone detection.
	A tool has been developed in Java and Python to support the experiments carried
	out and to validate the proposed approach.
	Finally, we provide some evaluation results and share the key stages of our future work.

\end{abstract}

\begin{IEEEkeywords}
	code clone detection, machine learning, neural networks, abstract syntax trees.
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle

\section{Introduction}

A code fragment that was copied and pasted with major or minor edits is called
software duplicate or code clone.
It has been studied that 30\% of code in most of the software companies are code
clones \cite{royandcordy}.
The most common complication of code clones is software maintenance.
Specifically, there could be a bug present in one of the duplicated fragments,
in this case, that bug has to be detected and fixed in other identical fragments.
The presence of duplicated but identical bugs in many locations within a piece
of software increases the difficulty of software maintenance.

Code clones can emerge in a software system due to various reasons
(Fig. \ref{fig:causes}) \cite{royandcordysec}.
One of them is copying and pasting of a code fragment that could provide a
reasonable starting point for the implementation.
Forking could be another cause of clones occurring.
The term forking is used by Kapser and Godfrey\cite{harmful} to mean the reuse
of similar solutions with the hope that they will be diverged significantly with
the evolution of the system.
For example, when creating a driver for a hardware family, a similar hardware
family may already have a driver, and thus can be reused with slight
modifications \cite{royandcordysec}.

Merging of two similar systems: Sometimes two software systems of similar
functionalities are merged to produce a new one.
Although these systems may have been developed by different teams, clones may
produce in the merged system because of the implementations of similar
functionalities in both systems \cite{royandcordysec}.

Significant efforts in writing reusable code: It is hard and time consuming to
write reusable code.
Perhaps, it is easier to maintain two cloned fragments than the efforts to
produce a general but probably more complicated solution \cite{royandcordysec}.

Lack of ownership of the code to be reused: Code may be borrowed or shared from
another subsystem which may not be modified because it may belong to a different
department or even may not be modifiable (access not granted and/or stored in
nonvolatile memory).
In such situations, the only way of reusing the existing code is to copy and
paste with required adaptations \cite{royandcordysec}.

There are not all causes of clones occurrence.

\begin{figure}[h]
	\centering
	\includegraphics[width=3.5in]{causes}
	\caption{Tree-diagram for the reasons of cloning}
	\label{fig:causes}
\end{figure}

Detecting clones is an important problem for software maintenance.
Despite the fact that previous research has demonstrated harmful impacts of code
cloning, this is not always the case. Also, it is not always necessary to
refactor clones.
Nonetheless, the ability to automatically detect two similar fragments of code
is critical in many areas, e.g. code simplification, code maintainability,
plagiarism detection, copyright infringement detection, malicious software
detection and detection of bug reports.
There are few clones detection techniques known and used in practice.
We favor Roy and Cordy’s classification by their \textquote{internal source code
	representation} e.g. text-, token-, tree-, graph- and metric-based
techniques \cite{royandcordysec}.

The purpose of our project is to develop an AI-based method for clone detection.
Our approach has several steps, firstly we are generating an Abstract Syntax Tree (AST).
After that we remove all nodes from AST that are not interesting to us.
Next step is creating representations of tree tokens for feeding them to
pre-trained recurrent neural network.
Last step of our approach is answer representation in human-readable format.

This article contains five sections.
Section II presents the research background and the related work.
Section III describes the tool that has been developed.
Section IV discusses the results obtained using our tool.
Finally, Section V concludes the article.

\section{Background and Related Work}

\subsection{Basic definitions}

Each article defines clones in its own ways.
Below are the common definitions which are used throughout this article.

\textbf{Code fragment} is a part of a source code needed to run a program.
It can contain a function or a method, blocks or sequences of statements \cite{defs}.

\textbf{Code clones} two or more fragments of code that are similar with respect
to a clone type \cite{defs}.

\textbf{Code class} is a set of clone pairs where each pairs is related by the
same relation between the two code fragments \cite{defs}.

\textbf{Clone candidate} is a clone pair reported by a clone detector \cite{defs}.

\subsection{Types of clones}
Generally there are four types of clones.

\textbf{Type-I}.
Fragments that are identical to each other except for variations in comments and
layout \cite{royandcordysec}\cite{itsyksonakhin}.

\textbf{Type-II}.
Fragments that are identical to each other except for variations and in identifiers
name and literal values in addition to Type-I \cite{royandcordysec}\cite{itsyksonakhin}.

\textbf{Type-III}.
Syntactically similar fragments that differ at the statement level.
The fragments have statements added, modified or removed with each other with
respect to each other in addition to clone Type-II \cite{royandcordysec}\cite{itsyksonakhin}.

\textbf{Type-IV}.
Fragments that are syntactically dissimilar, but implement the same
functionality \cite{royandcordysec}\cite{itsyksonakhin}.

Type I - III clones indicate textual similarity, whereas Type IV - semantic similarity.
Our tool supposed to detect first three types of clones.

There are an examples of different clone types on fig \ref{fig:clones})

\begin{figure}[h]
	\centering
	\includegraphics[width=3.5in]{clones}
	\caption{Types of clones}
	\label{fig:clones}
\end{figure}
\subsection{Code Clone Detection}

The code clone detection is a two-phase process which consists of transformation
and comparison phases.
First part is a source text transformation into an internal format, which allows
usage of more efficient comparison algorithm. Actual matches were detected during
the second phase.

The clone detection process begins from transforming source code into
representations suitable for accessing similarity.
As it has been already mentioned in Introduction, detection techniques can be
classified by their source code representation.

\textbf{Text-based techniques.}
Approaches of this type use little or no transformation of a source code before
an actual comparison.
In most cases, a raw source code is used directly in the clone detection process.
Techniques in this category differ in underlying string comparison algorithm.
Comparing calculated signatures per line is one possibility to identify for
matching substrings.

\textbf{Token-based techniques.}
This approach begins by transforming a source code into a sequence of tokens and
then scanned for duplicated sub-sequences of tokens, and the corresponding
original code is returned as clones.
Matching subsequences of tokens generally improves recognition power, but token
abstraction has a tendency to admit more false positives \cite{roycordykoschke}.

\textbf{Tree-based techniques.}
This approach measures similarity of subtrees in syntactic representations.
First of all, source programs are converted into parse trees or abstract syntax
trees, which can then be processed using either tree matching or structural
metrics to find clones.

\textbf{Graph-based techniques.}
This type of approach refers to semantic approaches and uses a static program
analysis to provide more precise information than a simple syntactic similarity.
In some approaches, a program is represented with a program dependency graph (PDG).
The nodes of this graph represent expressions and statements, while the edges
represent control and data dependencies.

\textbf{Metric-based techniques.}
This approach gathers a number of metrics for code fragments, and then compare
metrics vectors rather than code or ASTs directly.
Such technique involves fingerprinting functions, metrics calculated for syntactic
like a class, function, method or statement that provides values that can be
compared to find clones of these syntactic units.
In most cases, a source code is first parsed to an AST or Control Flow Graph (CFG)
representation to calculate the metrics.
Metrics are calculated from names, layouts, expressions and control flow of
functions.
A clone is defined as a pair of whole function bodies with similar metrics values.

\subsection{Neural Networks}
A neural network is a series of methods that attempt to identify underlying
relationships in a set of data by using a process that mimics the way the human
brain operates.
Neural networks have achieved state of the art performance on similarity tasks,
e.g. searching for similar pictures, photos and text, this is the main reason
why we are using neural networks in our approach.

Recurrent neural networks (RNN) can use their feedback connections to store
representations of recent input events in form of activations.
RNNs are naturally suited for variable-length inputs like sentences, especially
the Long Short-Term Memory model \cite{lstm}.
LSTM sequentially updates a hidden-state representation, but these steps also
rely on a memory cell containing four components: a memory state, an output gate
that determines how the memory state affects other units, as well as an input
gate (and forget gate) that controls what is being stored in memory based on
each new input and a current state.

Standard RNNs suffer from the vanishing gradient problem in which the backpropagated
gradients become vanishingly small over long sequences \cite{pascanu}.
The LSTM model is a solution to this problem.
Like the standard RNN, the LSTM sequentially updates a hidden-state representation,
but it introduces a memory state \(c_t\) and three gates that control the flow
of information through the time steps \cite{siamese}.

Siamese architecture of RNN was used for clone detection purposes \cite{siamese}.
This model is applied for assessing semantic similarity between sentences.
The Manhattan LSTM model is outlined in Fig. \ref{fig:siam} \cite{siamese}.
There are two LSTM networks, each of them process one of the sentences in a
given pair.
This model uses LSTM to read in word-vectors representing each input sentence
and employs its final hidden state as a vector representation for each sentence.
Subsequently, similarity between these representations is used as a predictor
of semantic similarity.

Siamese architecture is an architecture of energy-based models (EBM).
EBMs are used in situations where the energies for various configurations must
be compared in order to make a decision
(classification, verification, etc.) \cite{siamese}.
The advantage of EBMs over traditional probabilistic models, particularly
generative models, is that there is no need for estimating normalized probability
distributions over the input space.
The absence of normalization saves us from computing partition functions that
may be intractable.
It also gives us considerably more freedom in the choice of architectures for
the model \cite{loss_func}.

\begin{figure}[H]
	\centering
	\includegraphics[width=3.5in]{siamese}
	\caption{Siamese RNN}
	\label{fig:siam}
\end{figure}

\subsection{Related Work}

Neural network usage for clones detection tools is not a new approach.
Martin White et al. \cite{deeplearning} developed an approach based in part on
language models.
In their work they cast clone detection as a recursive learning procedure
designed to adequately represent fragments that serve as constituents of
higher-order components.
The purpose of deep learning is to synchronize the source code representation
that was used in the clone detection process with the manner in which the code
is conceptually organized.
In other words, Martin White et al. use deep learning on preprocessing step.

Hui-Hui Wei and Ming Li \cite{cdls} proposed an end-to-end deep feature learning
framework for clone detection, namely CDLH (Clone Detection with Learning to Hash),
which simultaneously learn hash functions and representations of code fragments
via AST-based LSTM to take into account both the lexical and syntactical aspects
of source codes.

S. Carter et al. \cite{telecom} proposed a solution in the form of the
\textquote{Self-Organizing Map}.
This performs a classification using an unsupervised training phase.
It had the additional advantage of preserving any topological relationships
between input vectors.


\section{Methods}

This section describes the proposed approach for clone detection in Java source code.
Our tool has been developed in Java and Python.
It accepts Java project as the input and separates the functions/methods presented in it.
Having defined the methods, we are building vector representations of tokens for every method.
These vectors are input vectors for pre-trained neural network that analyzes a
project for clones.

The overall process is carried out in three major stages: pre-processing, detection
and post-processing. All stages of our approach are presented with a process
illustrated on fig. \ref{fig:stages}.

\begin{figure}[h]
	\centering
	\includegraphics[width=3.5in]{stages}
	\caption{Stages of work}
	\label{fig:stages}
\end{figure}

\subsection{Pre-processing}

This stage includes the process of comments and whitespace removal.
All files are being scanned for AST creation. During the first step we are looking
for useless tokens (such as comments, white spaces etc.) and removing them.
At the final step we are creating vector representation of tokens (word2vec
model) \cite{word2vec}.
As another neural network was used for vector representation, it has to be trained.
We used a project with 1953 KLOC (number of thousand lines of code, LOC).

Another part of pre-processing is dataset generation.
Self-written, so-called code mutator was used.
Its essence is to generate an artificial clone, i.e. copy and paste lines of code,
or delete them.
A vector representation was also built for obtained tokens, and it was then
fed to neural network for training.
A large project (3507,5 KLOC) was used for network training.
All methods in selected projects were mutated for the subsequent datasets generation.

The training set for a Siamese network consists of triplets (\(x_1, x_2, y\)),
where \(x_1\) and \(x_2\) are sequences and \(y \in \{0,1\}\) indicates
whether \(x_1\) and \(x_2\) are similar (\(y = 1\)) or dissimilar (\(y = 0 \)).
The aim of training is to minimize the distance in an embedding space between
similar pairs and maximize the distance between dissimilar pairs.

For effective training counterexamples are needed.
In that case, it was considered that source code of the project and code
obtained from mutator are clones.
On the other hand, source codes of currently used project (part of it) and
another project were considered as not clones (counterexamples).

\subsection{Detection}

The granularity of our approach is method-level.
Methods can have different length, it means that vector representations for
different methods can have different dimensions. In order to make
\textquote{word embeddings} fixed-dimensional, a sequence-to-sequence (seq2seq)
model was used \cite{seq2seq}.
Sequence-to-sequence models have enjoyed great success in a variety of tasks, such
as machine translation, speech recognition, and text summarization.

\begin{figure}[h]
	\centering
	\includegraphics[width=3.5in]{seq2seq}
	\caption{Sequence-to-sequence}
	\label{fig:seq2seq}
\end{figure}

The idea behind a seq2seq model is to use one LSTM to read the input sequence
, one timestep at a time, to obtain large fixed-dimensional vector
representation, and then to use another LSTM to extract the output sequence
from that vector (fig. \ref{fig:seq2seq}).
The second LSTM is essentially a recurrent neural network language model
except that it is conditioned on the input sequence.
State of LSTM’s hidden cells at the last moment of the encoder roll-out was
used as fixed-dimensional vectors.

Dataset for training seq2seq model is the same dataset as for Siamese neural network.
We are using vector representations of 3507,5 KLOC project.
Softmax cross entropy was used as loss function.
Optimization of the parameters is done using Adam optimization \cite{adam} and
loss minimization.

Our Siamese network contains 10 layers of Bidirectional LSTM nodes.
The output obtained from the final BLSTM layer is projected through a single
densely connected feedforward layer.
To learn the input pair similarity, euclidean based contrastive loss is used.
Loss function is shown in equation \ref{eq:contrastive}.

\begin{equation}
	\label{eq:contrastive}
	L(x_1,x_2,y,w)=
	\frac{(1-y)*D_w^2-y*max^2\{0,m-D_w\}}{2}
\end{equation}

, where
\begin{itemize}
	\item \(m > 0\) --- a margin, that defines a radius around \(G_w(x)\).
	      \(G_w\) is a parametric function
	\item \(D_w\) --- euclidean distance between \(x_1\) and \(x_2\).
	\item \(x_1,x_2,y\) --- the labeled sample pair.
	\item \(w\) --- parameter \cite{contrastive}.
\end{itemize}

\subsection{Post-processing}

The output from the previous phase is presented in the form of clone pairs.
Obtained clone pairs are pairs of vector representations.
To present results in a human-readable format, obtained representations are
fed into decoder (second LSTM in seq2seq model).
After the previous step, obtained representations are mapped back to AST
tokens which have human-readable properties.
The identified clone methods, called \textquote{potential clone pairs}, are
clustered separately for each clone.
Clustering is the process of grouping clone pairs into classes or clusters, so
that clone pairs within a cluster are highly similar to one another, but very
dissimilar to clone pairs in other clusters.

\section{Results}

To evaluate the proposed approach, a source code of several Java projects has
been used.
The experimental analysis has been carried out with a small-sized project of
2 KLOC to a medium-sized project of 14 KLOC.
In total, for testing we selected five projects: three of them were fully
mutated and other two were kept unchanged.
On the current level of our approach we are mutating the projects and testing
them for clones.

Each model was trained at least 10 epochs on one compute node of Nvidia DGX-1.
The average training time per epoch for seq2seq model is 20 minutes and for
Siamese network - 5 minutes.

The model that is being trained searches code clones on the selected projects
in few minutes.
As a result, we found approximately 83\% of clones in each project.
Searching for clones in large projects, at the moment, is very labor-intensive
and resource-consuming.
or example, it will require about 54 billion iterations to scan the whole
project with 3507,5 KLOCs clones.

Current state of our project is work in progress.
In the next phase of our project we will be solving the problem of iterations
for a large amount of code.
To solve this, we have agreed to use Locality- Sensitive Hashing (LSH).
LSH is an algorithm for solving the approximate or exact Near Neighbor Search
in high dimensional spaces.
The nearest neighbor search problem is: given a set of \textit{n} points
\(P = \{p_1, ..., p_n\}\) in a metric space X with distance function d, pre-process
\textit{P} so as to efficiently answer queries for finding the point in
\textit{P} closest to a query point \(q \in X\) \cite{lstm}.

Basic LSH idea \cite{lsh}:
\begin{itemize}
	\item use LSH: a special hash function that would put points that are
	      close together to the same point
	\item if two points are close together in high-dimensional space, then
	      they should remain close together after some projection to a lower-dimensional space
\end{itemize}

This idea can be easily understood using the example showing in fig. \ref{fig:lsh}.
Two points that are close together on the sphere are also close together when
the sphere is projected onto the two dimensional page.
No matter how to rotate the sphere.
Two other points on the sphere that are far apart will, for some orientations
, be close together on the page, but it is more likely that the points will
remain far apart.

\begin{figure}[h]
	\centering
	\includegraphics[width=3.5in]{lsh}
	\caption{Two examples showing projections of two close (circles) and two
		distant (squares) points onto the printed page}
	\label{fig:lsh}
\end{figure}

We foresee on more stage in our future research where we will be improving
dataset for Siamese RNN training.
At the moment datasets generated through a self-written \textquote{mutator} and
obtained datasets have not got a high accuracy.
To improve the results of Siamese RNN working we have to generate datasets
with a higher accuracy.

As was previously mentioned BigCloneBench is a clone detection benchmark of clones.
It was created for testing clone detection tools. This dataset is suitable for
our tool testing.

\section{Conclusion}

In this paper we have proposed an AI-based approach to detect method-level
code clones.
Our method uses several recurrent neural networks on different stages of
clones detection.
We could improve the precision and reduce the total comparison cost by
increasing datasets accuracy and neural network parameters changing.

The main distinguishing feature of proposed method is using neural networks
for clone detection.
Comparing the proposed method with the methods from the literature, it can be
concluded that neural networks were not used at the clone search level, but
only at the level of data preparation and response processing.

We have implemented the above mentioned method in practise to scan a small
and medium sized project for code clones, and the results that we obtained
indicate that the proposed method is not at the final stage and can be
improved.
The early experiments prove that with help of this method we could find
nearly 80\% of clones.
However, for more accurate results it is necessary to conduct more experiments
, especially on real projects.

\begin{thebibliography}{1}
	\bibitem{royandcordy}
	Chanchal K. Roy and James R. Cordy,
	\emph{An Empirical Study of Function Clones in Open Source Software},
	WCRE '08 Proceedings of the 2008 15th Working Conference on Reverse
	Engineering. Pages 81-90

	\bibitem{harmful}
	Cory Kapser	and Michael W. Godfrey
	\emph{\textquote{Cloning Considered Harmful} Considered Harmful},
	Proceedings of the 13th Working Conference on Reverse Engineering, 2006

	\bibitem{royandcordysec}
	Chanchal K. Roy and James R. Cordy
	\emph{A Survey on Software Clone Detection Research},
	Technical report, Queen's University, 2007

	\bibitem{defs}
	Abdullah Sheneamer and Jugal Kalita
	\emph{A Survey of Software Clone Detection Techniques},
	International Journal of Computer Applications 137, 2016.

	\bibitem{itsyksonakhin}
	Marat Akhin and Vladimir Itsykson
	\emph{Clone detection: Why, what and how?},
	6th Central and Eastern European Software Engineering Conference, 2010

	\bibitem{roycordykoschke}
	Chanchal K. Roy and James R. Cordy and Rainer Koschke
	\emph{Comparsion and evaluation of code clone detection techniques and
		tools: A qulitative approach},
	Science of Computer Programming 74, 2009

	\bibitem{lstm}
	Sepp Hochreiter and Jurgen Schmidhuber
	\emph{Long Short-Term Memory},
	Neural Computation, 1997.

	\bibitem{siamese}
	Jonas Mueller and Aditya Thyagarajan
	\emph{Siamese Recurrent Architectures for Learning Sentence Similarity},
	Proceedings of the 30th AAAI Conference on Artificial Intelligence
	(AAAI), 2016.

	\bibitem{word2vec}
	Thomas Mikolov and Ilya Sutskever and Kai Chen and Greg S Corrado and
	Jeff Dean
	\emph{Distributed Representations of Words and Phrases and their
		Compositionality},
	Advances in Neural Information Processing Systems 26, 2013.

	\bibitem{seq2seq}
	Ilya Sutskever and Oriol Vinyals and Quoc V. Le
	\emph{Sequence to Sequence Learning with Neural Networks},
	NIPS, 2014.

	\bibitem{adam}
	Diederik P. Kingma, Jimmy Ba
	\emph{Adam: A Method for Stochastic Optimization},
	3rd International Conference for Learning Representations, 2015.

	\bibitem{contrastive}
	Raia Hadsell, Sumit Chopra, Yann LeCun
	\emph{Dimensionality Reduction by Learning an Invariant Mapping},
	IEEE Computer Society Conference on Computer Vision and Pattern
	Recognition, CVPR 2006.

	\bibitem{lsh}
	Piotr Indyk and Rajeev Motwani
	\emph{Approximate Nearest Neighbors: Towards Removing the Curse of
		Dimensionality},
	Theory of Computing, 2000

\end{thebibliography}

\end{document}
